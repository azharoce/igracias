$( document ).ready(function() {
	get_content();
	checkToken();
})

function checkToken(){
	$.ajax({
		url :global_host+'Auth/checkToken',
		dataType:'json',
		cache:false,
		contentType:false,
		processData:false,
		type:'GET',
		headers: {
			"token": localStorage.getItem('token'),
		},
		success: function(data) {
			if(data.status == true){
				if(document.URL != global_host+'home'){
					toastr['success']('Selamat datang');
					window.location.replace(global_host+'home');
				}
			}else{
				if(document.URL != global_host){
					toastr['error'](data.message);
					window.location.replace(global_host);
				}
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			$(".page-loader-wrapper").css("display","none");
			toastr['error']('Maaf Untuk saat ini system masih dalam pengembangan.');
		}
	});
}

function get_content(){
	var form_data= new FormData();
	form_data.append('view','login');
	form_data.append('dir','_login');
	$.ajax({
		url :global_host+'welcome/get_content',
		dataType:'json',
		cache:false,
		contentType:false,
		processData:false,
		data:form_data,
		type:'post',
		success: function(data) {
			// alert(data);
			// console.log(data.)
			// var html = "<?php echo $this->load->view('"+atob(data.view)+"')?>";
			$(".card").html(atob(data.view));
			var js_baru = '';
			$.each(data.opsi_js, function( index, value ) {
				js_baru +=' <script src="'+value+'"></script>';
			});

			$("#js_baru").html(js_baru);
			// alert(data._js);
			// if (data._js == null) {
			// 				// caption = 'meh';
			// 				alert(1);
			// 		} else {
			// 			alert(2);
							// caption = photo.caption.text;
							var status_file = fileExists(global_host+'/assets/_js_alumni/'+data._js);
							if(status_file==false)
							{
								$("#_js").html('');
							}
							else {
								$("#_js").html('<script src="'+global_host+'/assets/_js_alumni/'+data._js+'"></script>');
							}

					// }
			// $("#_js").html('	<script src="'+data._js+'"></script>');
			// console.log(data._js);
			// alert(data._js);
			// $(".page-loader-wrapper").css("display","block");
		},
		error: function(xhr, textStatus, errorThrown) {
			$(".page-loader-wrapper").css("display","none");
			toastr['error']('Maaf Untuk saat ini system masih dalam pengembangan.');
		}
	});
}

function change_content(view1,dir)
{
  $(".page-loader-wrapper").css("display","block");
  var form_data= new FormData();
  form_data.append('view',view1);
  form_data.append('dir',dir);
  // form_data.append('datatables',js_css);

  $.ajax({
      url :global_host+'welcome/get_content',
      dataType:'json',
      cache:false,
      contentType:false,
      processData:false,
      data:form_data,
      type:'post',
      success: function(data) {
        // $(".card").html(data);
				console.log(data);
				var a =atob(data.view);

        $(".card").html(a);
        // var js_baru = '';
        $.each(data.opsi_js, function( index, value ) {
          js_baru +=' <script src="'+value+'"></script>';
        });
        // console.log(a);
        $("#js_baru").html(js_baru);
				var status_file = fileExists(global_host+'/assets/_js_alumni/'+data._js);
				if(status_file==false)
				{
					$("#_js").html('');
				}
				else {
					$("#_js").html('<script src="'+global_host+'/assets/_js_alumni/'+data._js+'"></script>');
				}
				// alert(data._js);
        $(".page-loader-wrapper").css("display","none");
      },
      error: function(xhr, textStatus, errorThrown) {
        $(".page-loader-wrapper").css("display","none");
		toastr['error']('Failed Request Content.');
      }
  });
}
function change(view,dir,js_css)
{
  $(".page-loader-wrapper").css("display","block");
  var form_data= new FormData();
  form_data.append('view',view);
  form_data.append('dir',dir);
  form_data.append('js_css',js_css);
 // console.log(js_css);
  $.ajax({
    url :global_host+'welcome/get_content',
      dataType:'json',
      cache:false,
      contentType:false,
      processData:false,
      data:form_data,
      type:'post',
      success: function(data) {
        // var html = "<?php echo $this->load->view('"+atob(data.view)+"')?>";
        $("#main-content").html(atob(data.view));
        var js_baru = '';
        $.each(data.opsi_js, function( index, value ) {
          js_baru +=' <script src="'+value+'"></script>';
        });
        var css_baru ='';
        $.each(data.opsi_css, function( index, v ) {
          css_baru +='<link rel="stylesheet" type="text/css" href="'+v+'">';
        });
				$("#js_baru").html(js_baru);

				// alert(1);
				var status_file = fileExists(global_host+'/assets/_js_alumni/'+data._js);
				if(status_file==false)
				{
					$("#_js").html('');
				}
				else {
					$("#_js").html('<script src="'+global_host+'/assets/_js_alumni/'+data._js+'"></script>');
				}
        $(".page-loader-wrapper").css("display","none");
      },
      error: function(xhr, textStatus, errorThrown) {
        $(".page-loader-wrapper").css("display","none");
    toastr['error']('Failed Request Content.');
      }
  });
}
function fileExists(url) {
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        return req.status==200;
    } else {
        return false;
    }
}
