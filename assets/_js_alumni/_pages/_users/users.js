$(document).ready(function() {
	// $(".page-loader-wrapper").css("display","block");
	// alert(checkToken());
	var form_data= new FormData();
	form_data.append('table','tb_users');
	// console.log(localStorage.getItem('token'));
	// localStorage.setItem('token',data.data.token)
	// console.log(checkToken());
	$.ajax({
		url :global_host+'_api/_tb_master/data_master',
		dataType:'json',
		cache:false,
		contentType:false,
		processData:false,
		data:form_data,
		headers: {
			 "Authorization": localStorage.getItem('token')
		 },
		type:'post',
		success: function(data) {
			// var html = "<?php echo $this->load->view('"+atob(data.view)+"')?>";
			alert(a);

		},
		error: function(xhr, textStatus, errorThrown) {
        $(".page-loader-wrapper").css("display","none");
			toastr['error']('Maaf Untuk saat ini system masih dalam pengembangan.');
		}
	});

	var dataSet = [
    [ "Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25" ],
    [ "Unity Butler", "Marketing Designer", "San Francisco", "5384", "2009/12/09" ]
];

var table = $('#s').DataTable( {
			 "ajax": "https://thememakker.com/templates/lucid/html/light/assets/data/objects.txt",
			 "columns": [
					 {
							 "className":      'details-control',
							 "orderable":      false,
							 "data":           null,
							 "defaultContent": ''
					 },
					 { "data": "name" },
					 { "data": "position" },
					 { "data": "office" },
					 { "data": "salary" }
			 ],
			 "order": [[1, 'asc']]
	 } );
	 $('#s tbody').on('click', 'td.details-control', function () {
	        var tr = $(this).closest('tr');
	        var row = table.row( tr );

	        if ( row.child.isShown() ) {
	            // This row is already open - close it
	            row.child.hide();
	            tr.removeClass('shown');
	        }
	        else {
	            // Open this row
	            row.child( format(row.data()) ).show();
	            tr.addClass('shown');
	        }
	    } );
})
