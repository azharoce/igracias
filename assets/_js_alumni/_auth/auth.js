function doLogin()
{
     var username = $("#signin-email").val();
     var password = $("#signin-password").val();
     var form_data= new FormData();

    form_data.append('username',username);
    form_data.append('password',password);

    $.ajax({
		url :global_host+'auth',
		dataType:'json',
		cache:false,
		contentType:false,
		processData:false,
		data:form_data,
		type:'post',
		success: function(data) {
			if (data.status==true) {
				localStorage.setItem('token',data.data.token)
				checkToken();
			}
			else
			{
				toastr['error']('Failed login.');
			}
			$(".page-loader-wrapper").css("display","none");
		},
		error: function(xhr, textStatus, errorThrown) {
			$(".page-loader-wrapper").css("display","none");
			toastr['error']('Failed Request Content.');
		}
	});

}
