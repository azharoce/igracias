$(document).ready(function() {
	$(".page-loader-wrapper").css("display","block");
	var form_data= new FormData();
	form_data.append('view','home');
	form_data.append('dir','_dashboard');
	form_data.append('js_css','dashboard');
	$.ajax({
		url :global_host+'welcome/get_content',
		dataType:'json',
		cache:false,
		contentType:false,
		processData:false,
		data:form_data,
		type:'post',
		success: function(data) {
			// var html = "<?php echo $this->load->view('"+atob(data.view)+"')?>";
			$("#main-content").html(atob(data.view));
			var js_baru = '';
			$.each(data.opsi_js, function( index, value ) {
				js_baru +=' <script src="'+value+'"></script>';
			});

			$("#js_baru").html(js_baru);
			$(".page-loader-wrapper").css("display","none");
		},
		error: function(xhr, textStatus, errorThrown) {
        $(".page-loader-wrapper").css("display","none");
			toastr['error']('Maaf Untuk saat ini system masih dalam pengembangan.');
		}
	});  


	// var form_data_test= new FormData();
	// form_data_test.append('table','tb_role');
	// alert(token);
	// $.ajax({
		// url :global_host+'_api/_tb_master/data_master/',
		// dataType:'json',
		// cache:false,
		// contentType:false,
		// processData:false,
		// data:form_data_test,
		// type:'post',
		// headers: {
			// "Authorization":token,
		// },
		// success: function(data) {
			// console.log(data);
			// var html = "<?php echo $this->load->view('"+atob(data.view)+"')?>";
		// },
		// error: function(xhr, textStatus, errorThrown) {
			// $(".page-loader-wrapper").css("display","none");
			// toastr['error']('Maaf Untuk saat ini system masih dalam pengembangan.');
		// }
	// });
})