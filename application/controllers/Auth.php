<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// require APPPATH . '/libraries/REST_Controller.php';

class Auth extends MY_Controller
{
	
	protected $auth = 'Auth';
	
    function __construct()
    {
        parent::__construct();

        $this->load->model('_auth/Auth_model');
    }
    public function index_post()
    {		
        $tokenData = array();
		$date = new DateTime();

        // start data post
        $username = $this->post('username');
        $password = md5($this->post('password'));
				
        // end data post
        $status = false;
		$output['token'] = false;				
        if ($this->Auth_model->get_userdata($username,$password)) {
            $data = $this->Auth_model->get_userdata($username,$password);        
           
            $tokenData['id'] = $data['id']; //TODO: Replace with data for token

            $data_role = $this->Auth_model->get_role($data['id']);
			
			$roles = array();
			foreach($data_role as $i => $v){
				$roles[] = $v['id_role'];
			}
            $data['roles'] = $roles;

            $data_alumni = $this->Auth_model->get_data_alumni($data['id_alumni']);
            $data['data_alumni'] = $data_alumni;
			$data['iat'] = $date->format('U');
			$data['exp'] = ($date->format('U') + 60*60*24*1); // Expired token 3 Hari

            $status = true;
			// print_r($data);
			// exit;
			if($data['flag'] == 'Y'){
				$output['token'] = AUTHORIZATION::generateToken($data);				
			}
        }
		$this->response(array("status" => $status, 'data' => $output));
    }
	
	function checkToken_get(){
		$data = $this->validate();
		
		echo json_encode($data);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
    function data_get()
    {
		$listData = $this->validate();
		$this->response($listData);
    }
	
     function logout_get()
    {
        $data = $this->session->sess_destroy();
       // $this->set_response($data, REST_Controller::HTTP_OK);

    }

    public function token_get()
    {
        $tokenData = array();
        $tokenData['id'] = 1; //TODO: Replace with data for token
        $tokenData['timestamp'] = now();
        
        $output['token'] = AUTHORIZATION::generateToken($tokenData);
        $this->set_response($output, REST_Controller::HTTP_OK);
    }

    public function token_post()
    {
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                return;
            }
        }

        $this->set_response("Unauthorised", REST_Controller::HTTP_UNAUTHORIZED);
    }
}
