<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
  {
       parent::__construct();
			 $this->data['css'] = $this->layout->css_all();
			 $this->data['js'] = $this->layout->js_all();
  }
	public function forgot_password()
	{
			$view[] ='_pages/_login/forgot_password';
			$this->layout->_render_page_log($view,$this->data);
	}
  public function errors()
  {
      $view[] ='_pages/_login/error';
      $this->layout->_render_page_log($view,$this->data);
  }
  public function registrasi()
  {
      $view[] ='_pages/_login/error';
      $this->layout->_render_page_log($view,$this->data);
  }
}
