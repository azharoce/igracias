  <?php

defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * Changes:
 * 1. This project contains .htaccess file for windows machine.
 *    Please update as per your requirements.
 *    Samples (Win/Linux): http://stackoverflow.com/questions/28525870/removing-index-php-from-url-in-codeigniter-on-mandriva
 *
 * 2. Change 'encryption_key' in application\config\config.php
 *    Link for encryption_key: http://jeffreybarke.net/tools/codeigniter-encryption-key-generator/
 *
 * 3. Change 'jwt_key' in application\config\jwt.php
 * 3. Change 'token_timeout' in application\config\jwt.php
 *
 */

class Data_master extends REST_Controller
{
  function __construct()
  {
      parent::__construct();

      $this->load->model('_tb_master/Master_model');
  }
    public function index_post()
    {
      $headers = $this->input->request_headers();

      if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
          $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
            $table = $this->input->post('table');
            if(isset($table))
            {
                $a = $this->Master_model->get($table);
                $hasil = array(
                  'status'=>false,
                  'data' =>$a
                );
                $this->set_response($hasil, REST_Controller::HTTP_OK);
            }
            else
            {
                $hasil = array(
                  'status'=>true,
                  'message' =>'please check parameters request '
                );
            }
            return;
          }
      }
      $this->set_response("Unauthorised", REST_Controller::HTTP_UNAUTHORIZED);
    }
    public function insert_post($table)
    {
      $headers = $this->input->request_headers();

      if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
          $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
            $post = $this->input->post();
            $a = $this->Master_model->insert($table,$post);
           // print_r($a);
            $this->set_response($decodedToken, REST_Controller::HTTP_OK);
             return;
          }
      }
      $this->set_response("Unauthorised", REST_Controller::HTTP_UNAUTHORIZED);
    }
    public function update_post($table)
    {
      $headers = $this->input->request_headers();

      if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
          $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
            $post = $this->input->post();
            $a = $this->Master_model->insert($table,$post);
           // print_r($a);
            $this->set_response($decodedToken, REST_Controller::HTTP_OK);
             return;
          }
      }
      $this->set_response("Unauthorised", REST_Controller::HTTP_UNAUTHORIZED);
    }
}
