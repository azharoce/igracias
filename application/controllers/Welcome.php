<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require APPPATH . '/libraries/REST_Controller.php';
class Welcome extends CI_Controller {
	public function index()
	{
		$valid = $this->lib_auth->validate();
		$this->data['css'] = $this->layout->css_all();
		$this->data['js'] = $this->layout->js_all();
		$view[] ='_pages/_login/index';
		$this->data['opsi_js'] =array('_js_alumni/_auth/auth.js');
		$this->layout->_render_page_log($view,$this->data);
	}

	function get_content()
	{
		$view =$this->input->post('view');
		$dir =$this->input->post('dir');
		// echo $this->input->post('js_css');
		if($this->input->post('js_css')=='dashboard')
		{
			$js = $this->layout->js_dashboard();
			$css =$this->layout->css_dashboard();

		}
		elseif ($this->input->post('js_css')=='datatables') {
			$js = $this->layout->js_datatables();
			$css =$this->layout->css_datatables();

		}
		else
		{
			$js = array();
			$css = array();
		}
		// print_r($js_css);
		$b = $this->load->view('_pages/'.$dir.'/'.$view,'',TRUE);
		$a = base64_encode($b);
		$b ='_pages/'.$dir.'/'.$view.'.js';
		$data = array(
			'opsi_css' => $css,
			'opsi_js' => $js,
			'view' =>$a,
			'_js' =>$b
		);
		echo json_encode($data);
	}
	function login()
	{
		$this->load->view('_pages/_login/index');

	}
	function landing()
	{
		$this->load->view('welcome_message');
	}
	function test()
	{
		header('Content-Type: application/json');
		 echo  '{
  "data": [
    [
      "Tiger Nixon",
      "System Architect",
      "Edinburgh",
      "5421",
      "2011/04/25",
      "$320,800"
    ],
    [
      "Donna Snider",
      "Customer Support",
      "New York",
      "4226",
      "2011/01/25",
      "$112,000"
    ]
  ]
}';
// echo json_encode();
	}
}
