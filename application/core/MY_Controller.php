<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class MY_Controller extends REST_Controller {
  function __construct()
  {
       parent::__construct();
	   	if(!isset($this->auth)){
		   $this->validate();
		}
  }

  	public function validate(){
		$date = new DateTime();
		$token = $this->input->get_request_header('token');
		if($token != NULL){
			$decodedToken = AUTHORIZATION::validateToken($token);
			$date_now = $date->format('U');
			if($decodedToken == true){
				if($date_now <= $decodedToken->exp){
					$decodedToken = json_decode(json_encode($decodedToken),true);
					return array("status" => true, "data" => $decodedToken);
				} else {
					return array("status" => false, "message" => "Token is Expired");
				}
			} else {
				return array("status" => false, "message" => "Token Not Valid");
			}
		} else {
			return array("status" => false, "message" => "Token Not Found");
		}
	}	

	// public function payload($data = ''){
		// $date = new DateTime();
		// $token = $this->input->get_request_header('token');
		// if($token != NULL){
			// $decodedToken = AUTHORIZATION::validateToken($token);
			// $date_now = $date->format('U');
			// if($decodedToken == true){
				// if($date_now <= $decodedToken->exp){
					// $decodedToken = json_decode(json_encode($decodedToken),true);
					// if($data){
						// $data = strtolower($data);
						// return $decodedToken[$data];
					// }else{
						// return $decodedToken;
					// }
					// exit;
				// } else {
				// }
			// } else {
			// }
		// } else {
		// }
	// }


}
