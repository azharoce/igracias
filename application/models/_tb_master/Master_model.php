<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * Changes:
 * 1. This project contains .htaccess file for windows machine.
 *    Please update as per your requirements.
 *    Samples (Win/Linux): http://stackoverflow.com/questions/28525870/removing-index-php-from-url-in-codeigniter-on-mandriva
 *
 * 2. Change 'encryption_key' in application\config\config.php
 *    Link for encryption_key: http://jeffreybarke.net/tools/codeigniter-encryption-key-generator/
 *
 * 3. Change 'jwt_key' in application\config\jwt.php
 * 3. Change 'token_timeout' in application\config\jwt.php
 *
 */

class Master_model extends CI_Model
{
    function check_table($table)
    {
        $data = $this->db->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA ='alumni' and TABLE_NAME ='$table' ")->result_array();
        $hasil =0;
        if(count($data)!=0)
        {
           $hasil = 1;
        }
        return $hasil;
    }
    function get($table,$batas=null,$start=null)
    {
       if($this->check_table($table)==1)
       {
            $data = $this->db->get($table)->result_array();
        }
        else
        {
          $data = array('Messages'=>'Not Found In Database');
        }
        return $data;

    }
    function insert($table,$post)
    {
          if($this->check_table($table)==1)
          {
              $data = $this->db->insert($table,$post);
          }
          else
          {
              $data = array('Messages'=>'Not Found In Database');
          }
          return $data;

    }

    function delete($table,$id)
    {
          if($this->check_table($table)==1)
          {
              $data = $this->db->where('id')->delete($table);
          }
          else
          {
              $data = array('Messages'=>'Not Found In Database');
          }
          return $data;

    }

}
