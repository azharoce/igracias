<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    function get_userdata($username,$password)
    {
        $data = $this->db
                ->where('username',$username)
                ->where('password',$password)
                ->get('tb_users')
                ->row_array();
        return $data;
    }
    function get_role($id)
    {  
        $data = $this->db 
                ->where('id_user',$id)
                ->from('tb_user_role')
                ->join('tb_role','tb_role.id=tb_user_role.id_role')
                ->get()
                ->result_array();
        return $data;
    }
    function get_data_alumni($id)
    {
        $data = $this->db 
                ->where('id',$id)
                ->get('tb_alumni')
                ->row_array();
        return $data;
    }
}
