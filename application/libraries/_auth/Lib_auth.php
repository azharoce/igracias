<?php
/**
 *
 */
class Lib_auth
{
	function __construct()
	{
		$this->apps =& get_instance();
	}

	public function validate(){
		$date = new DateTime();
		$token = $this->apps->input->get_request_header('token');
		if($token != NULL){
			$decodedToken = AUTHORIZATION::validateToken($token);
			$date_now = $date->format('U');
			if($decodedToken == true){
				if($date_now <= $decodedToken->exp){
					$decodedToken = json_decode(json_encode($decodedToken),true);
					return array("status" => true, "data" => $decodedToken);
				} else {
					return array("status" => false, "message" => "Token is Expired");
				}
			} else {
				return array("status" => false, "message" => "Token Not Valid");
			}
		} else {
			return array("status" => false, "message" => "Token Not Found");
		}
	}

}

 ?>
