<?php
Class Layout
{
    public function __construct()
    {
        $this->apps =& get_instance();
    }
    function _render_page($view, $data=null)
  	{
  			$this->viewdata = (empty($data)) ? $this->data: $data;
  			$this->apps->load->view('_layout/header',$this->viewdata);
        $this->apps->load->view('_layout/menu');

  			for ($i=0; $i <count($view) ; $i++)
  			{
  					$view_html = $this->apps->load->view($view[$i]);
  			}
  			$this->apps->load->view('_layout/footer');
  	}
    function _render_page_log($view, $data=null)
  	{
  			$this->viewdata = (empty($data)) ? $this->data: $data;
  			$this->apps->load->view('_layout/header',$this->viewdata);
  			for ($i=0; $i <count($view) ; $i++)
  			{
  					$view_html = $this->apps->load->view($view[$i]);
  			}
  			$this->apps->load->view('_layout/footer');
  	}
    public function css_all()
    {
        $data = array(
            base_url('assets/vendor/bootstrap/css/bootstrap.min.css'),
            base_url('assets/vendor/font-awesome/css/font-awesome.min.css'),
            base_url('assets/vendor/toastr/toastr.min.css'),
            base_url('assets/css/main.css'),
            base_url('assets/css/custom.css'),
            base_url('assets/css/color_skins.css'),
        );
        return $data;
    }
    public function js_all()
    {
        $data = array(
            base_url('assets/bundles/libscripts.bundle.js'),
            base_url('assets/bundles/vendorscripts.bundle.js'),
            base_url('assets/vendor/toastr/toastr.js'),
            base_url('assets/bundles/mainscripts.bundle.js'),
            base_url('assets/_js_alumni/_global/global_js.js'),
            // base_url('assets/_js_alumni/_auth/auth.js'),
        );
        return $data;
    }
    public function js_dashboard()
    {
        $data = array(
            base_url('assets/bundles/chartist.bundle.js'),
            base_url('assets/bundles/knob.bundle.js'),
            base_url('assets/js/index.js'),
        );
        return $data;
    }
    public function css_dashboard()
    {
        $data = array(
            base_url('assets/vendor/chartist/css/chartist.min.css'),
            base_url('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css'),
        );
        return $data;
    }

    public function css_datatables()
    {
        $data = array(
            // base_url('assets/vendor/chartist/css/chartist.min.css'),
            // base_url('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css'),
            base_url('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css'),
base_url('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css'),
base_url('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css'),
base_url('assets/vendor/sweetalert/sweetalert.css'),
        );
        return $data;
    }

    public function js_datatables()
    {
        $data = array(
            base_url('assets/bundles/datatablescripts.bundle.js'),
            base_url('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js'),
            base_url('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js'),
            base_url('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js'),
            base_url('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js'),
            base_url('assets/vendor/jquery-datatable/buttons/buttons.print.min.js'),
            base_url('assets/vendor/sweetalert/sweetalert.min.js'), 
            base_url('assets/bundles/mainscripts.bundle.js'),
            base_url('assets/js/pages/tables/jquery-datatable.js'),
        );
        return $data;
    }
    
   
}
?>