<!doctype html>
<html lang="en">
<head>
<title>IAPMP</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<meta name="description" content="Portal Alumni Perguruan Muhammadiyah Pantenan">
<meta name="author" content="Azharuddin Arrosis">


<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<?php foreach ($css as $all) { ?>
          <link href="<?=$all?>" rel="stylesheet">
<?php } ?>
<!-- <div id="opsi_css"></div> -->
<!-- <link rel="stylesheet" type="text/css" href="#" id="opsi_css"> -->
<?php if(isset($other_css)){ ?>

<?php foreach ($other_css as $all) { ?>
          <link href="<?=$all?>" rel="stylesheet">
<?php } ?>
<?php } ?>
</head>
<body class="theme-blue">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-0"><img src="<?=base_url('assets/img/muhammadiyah.svg')?>" width="300" height="300" alt="Lucid"></div>
        <p>Please wait...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->

<div id="wrapper">
