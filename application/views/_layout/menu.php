<nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
        </div>

        <div class="navbar-brand">
            <a href="index-2.html"><img src="assets/img/admin-logo.svg" alt="Lucid Logo" class="img-responsive logo"></a>
        </div>

        <div class="navbar-right">
            <form id="navbar-search" class="navbar-form search-form">
                <input value="" class="form-control" placeholder="Search here..." type="text">
                <button type="button" class="btn btn-default"><i class="icon-magnifier"></i></button>
            </form>

            <div id="navbar-menu">
                <ul class="nav navbar-nav">
                    <li><a href="app-events.html" class="icon-menu d-none d-sm-block d-md-none d-lg-block"><i class="icon-calendar"></i></a></li>
                    <li><a href="app-chat.html" class="icon-menu d-none d-sm-block"><i class="icon-bubbles"></i></a></li>
                    <li><a href="app-inbox.html" class="icon-menu d-none d-sm-block"><i class="icon-envelope"></i><span class="notification-dot"></span></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="icon-bell"></i>
                            <span class="notification-dot"></span>
                        </a>
                        <ul class="dropdown-menu notifications">
                            <li class="header"><strong>You have 4 new Notifications</strong></li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="icon-info text-warning"></i>
                                        </div>
                                        <div class="media-body">
                                            <p class="text">Campaign <strong>Holiday Sale</strong> is nearly reach budget limit.</p>
                                            <span class="timestamp">10:00 AM Today</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="icon-like text-success"></i>
                                        </div>
                                        <div class="media-body">
                                            <p class="text">Your New Campaign <strong>Holiday Sale</strong> is approved.</p>
                                            <span class="timestamp">11:30 AM Today</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                             <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="icon-pie-chart text-info"></i>
                                        </div>
                                        <div class="media-body">
                                            <p class="text">Website visits from Twitter is 27% higher than last week.</p>
                                            <span class="timestamp">04:00 PM Today</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="icon-info text-danger"></i>
                                        </div>
                                        <div class="media-body">
                                            <p class="text">Error on website analytics configurations</p>
                                            <span class="timestamp">Yesterday</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="footer"><a href="javascript:void(0);" class="more">See all notifications</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-equalizer"></i></a>
                        <ul class="dropdown-menu user-menu menu-icon">
                            <li class="menu-heading">ACCOUNT SETTINGS</li>
                            <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Basic</span></a></li>
                            <li><a href="javascript:void(0);"><i class="icon-equalizer"></i> <span>Preferences</span></a></li>
                            <li><a href="javascript:void(0);"><i class="icon-lock"></i> <span>Privacy</span></a></li>
                            <li><a href="javascript:void(0);"><i class="icon-bell"></i> <span>Notifications</span></a></li>
                            <li class="menu-heading">BILLING</li>
                            <li><a href="javascript:void(0);"><i class="icon-credit-card"></i> <span>Payments</span></a></li>
                            <li><a href="javascript:void(0);"><i class="icon-printer"></i> <span>Invoices</span></a></li>
                            <li><a href="javascript:void(0);"><i class="icon-refresh"></i> <span>Renewals</span></a></li>
                        </ul>
                    </li>
                    <li><a href="page-login.html" class="icon-menu"><i class="icon-login"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            <img src="assets/img/user.png" class="rounded-circle user-photo" alt="User Profile Picture">
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong><?=$this->session->userdata('data_alumni')['first_name']?></strong></a>
                <ul class="dropdown-menu dropdown-menu-right account">
                    <li><a href="#"  onclick="change('users','_users')"><i class="icon-user"></i>My Profile</a></li>
                    <li><a href="app-inbox.html"><i class="icon-envelope-open"></i>Messages</a></li>
                    <li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>
                    <li class="divider"></li>
                    <li><a href="page-login.html"><i class="icon-power"></i>Logout</a></li>
                </ul>
            </div>
            <hr>
            <div class="row">
                <div class="col-4">
                    <h6>5+</h6>
                    <small>Experience</small>
                </div>
                <div class="col-4">
                    <h6>400+</h6>
                    <small>Employees</small>
                </div>
                <div class="col-4">
                    <h6>80+</h6>
                    <small>Clients</small>
                </div>
            </div>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home"><i class="icon-home"></i></a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#project_menu"><i class="icon-folder-alt"></i></a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#sub_menu"><i class="icon-grid"></i></a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#setting"><i class="icon-settings"></i></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content p-l-0 p-r-0">
            <div class="tab-pane active" id="home">
                <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                        <li>
                            <a href="#Employees" class="has-arrow"><i class="fa fa-history"></i><span>Rekam Jejak</span></a>
                            <ul>
                                <li><a href="emp-all.html">Pendidikan</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="tab-pane" id="project_menu">
                <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                     
                        <li>
                            <a href="#Projects" class="has-arrow"><i class="icon-list"></i><span>Projects</span></a>
                            <ul>
                                <li><a href="project-add.html">Add Projects</a></li>
                                <li><a href="project-list.html">Projects List</a></li>
                                <li><a href="project-detail.html">Projects Detail</a></li>
                            </ul>
                        </li>
                       
                    </ul>
                </nav>
            </div>
            <div class="tab-pane" id="sub_menu">
                <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                        <li>
                            <a href="#Widgets" class="has-arrow"><i class="icon-puzzle"></i><span>Widgets</span></a>
                            <ul>
                                <li><a href="widgets-statistics.html">Statistics Widgets</a></li>
                                <li><a href="widgets-data.html">Data Widgets</a></li>
                                <li><a href="widgets-chart.html">Chart Widgets</a></li>
                                <li><a href="widgets-weather.html">Weather Widgets</a></li>
                                <li><a href="widgets-social.html">Social Widgets</a></li>
                            </ul>
                        </li>
                     
                    </ul>
                </nav>
            </div>
            <div class="tab-pane p-l-15 p-r-15" id="setting">
                 <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                        <!-- <li><a href="index2.html"><i class="icon-speedometer"></i><span>Role</span></a></li> -->
                        <li>
                            <a href="#Clients" class="has-arrow"><i class="icon-users"></i><span>User Management</span></a>
                            <ul>
                                <li><a href="#"  onclick="change('users','_users','datatables')">Users</a></li>
                                <li><a href="#"  onclick="change('home','_dashboard','dashboard')">Role</a></li>
                                <li><a href="client-detail.html">Clients Detail</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#Clients" class="has-arrow"><i class="icon-lock"></i><span>Permission</span></a>
                            <ul>
                                <li><a href="client-add.html">Set Role</a></li>
                                <li><a href="client-list.html">Roles</a></li>
                                <li><a href="client-detail.html">Clients Detail</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#Clients" class="has-arrow"><i class="icon-lock"></i><span>Themes Setting</span></a>
                            <ul>
                                <li><a href="client-add.html">Change Themes</a></li>
                            </ul>
                        </li>
                       
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
