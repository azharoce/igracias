
<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from thememakker.com/templates/lucid/landing/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 03 Aug 2018 02:36:23 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>:: Lucid :: Marketing Page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link rel="stylesheet" href="<?=base_url('assets/css/normalize.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/jquery.fancybox.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/flexslider.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/styles.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/queries.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/bower_components/animate.css/animate.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/font-awesome.min.css')?>">
    <script src="<?=base_url('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')?>"></script>
</head>
<body id="top">
    <section class="hero">
        <section class="navigation">
            <header>
                <div class="header-content">
                    <div class="logo"><a href="#"><img src="<?=base_url('assets/img/logo.png')?>" alt="Lucid logo"></a></div>
                    <div class="header-nav">
                        <nav>
                            <ul class="primary-nav">
                                <li><a href="https://thememakker.com/templates/lucid/documentation/index.html" target="_blank">Documentation</a></li>
                                <li><a href="#features">Assets</a></li>
                                <li><a href="#assets">Features</a></li>
                                <li><a href="#demo">Demo</a></li>
                            </ul>
                            <ul class="member-actions">
                                <li><a href="https://thememakker.com/templates/lucid/html/light/page-login.html" target="_blank" class="login">Log in</a></li>
                                <li><a href="https://themeforest.net/item/lucid-powerful-bootstrap-4-admin-webapp-template/22212271" class="btn-white btn-small">Buy Now!</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="navicon">
                        <a class="nav-toggle" href="#"><span></span></a>
                    </div>
                </div>
            </header>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Lucid Bootstrap 4x</h1>
                        <h2>Everything you need for your webapp.</h2>
                        <p class="intro">Easy and elegant interface which gives you an outstanding experience.<br>
                            Take a stab at Lucid demos and find it out by yourself.</p>
                        <a href="https://thememakker.com/templates/lucid/html/light/index.html" target="_blank" class="btn btn-fill btn-large">Light</a>
                        <a href="https://thememakker.com/templates/lucid/html/dark/index.html" target="_blank" class="btn btn-accent btn-large">Dark</a>
                        <a href="https://thememakker.com/templates/lucid/html/rtl/index.html" target="_blank" class="btn btn-accent btn-large">RTL</a>
                        <a href="https://thememakker.com/templates/lucid/html/h-menu/index.html" target="_blank" class="btn btn-accent btn-large">Horizontal Menu</a>
                        <a href="https://thememakker.com/templates/lucid/html/mini-sidebar/index.html" target="_blank" class="btn btn-accent btn-large">Mini Sidebar</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="down-arrow floating-arrow"><a href="#"><i class="fa fa-angle-down"></i></a></div>
    </section>
    <section class="intro section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <i class="fa fa-gears fa-2x"></i>
                    </div>
                    <div class="intro-content">
                        <h5>Easy to customize</h5>
                        <p>Customize the code as per your need. Fully flexible and easily understandable code which anybody can modify by oneself.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <i class="fa fa-calendar fa-2x"></i>
                    </div>
                    <div class="intro-content last">
                        <h5>Calendar integration</h5>
                        <p>Save your events, meetings and important reminders. Calendar is also having ability to integrate google events with some extra coding.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <i class="fa fa-thumbs-up fa-2x"></i>
                    </div>
                    <div class="intro-content">
                        <h5>Free Update</h5>
                        <p>Update! Update! Update! Improvement is must in current time in any of changing world Get the latest updates in timly interval at no extra cost.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <i class="fa fa-leaf fa-2x"></i>
                    </div>
                    <div class="intro-content">
                        <h5>Clean User Interface</h5>
                        <p>Clean, Unique and Fresh UI which will catch anyone's eye to grab on the design. Great UI creats a great impact!</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <i class="fa fa-puzzle-piece fa-2x"></i>
                    </div>
                    <div class="intro-content last">
                        <h5>Ready to use widget</h5>
                        <p>10+ dashboards power packed with lots of widgets. Choose the best one according to your need.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <i class="fa fa-money fa-2x"></i>
                    </div>
                    <div class="intro-content">
                        <h5>Save Time & Money</h5>
                        <p>We've already invested time to build a great theme keeping requirement of our clients in mind which will definitely save your time & money.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="features section-padding" id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-7">
                    <div class="feature-list">
                        <h3>Lucid will represent your product exceptionally outstanding</h3>
                        <p>Highlight your product, web presence, or profile in a beautifully modern way.</p>
                        <ul class="features-stack">
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <i class="fa fa-laptop fa-2x"></i>
                                </div>
                                <div class="feature-content">
                                    <h5>Responsive Design</h5>
                                    <p>Bootstrap enabled template fits into all the devices and looks consistent on Mobile, Tablet and Desktops</p>
                                </div>
                            </li>
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <i class="fa fa-list fa-2x"></i>
                                </div>
                                <div class="feature-content">
                                    <h5>Expanded and Collapsed Menu</h5>
                                    <p>Lucid menu that works in all devices from mobile to desktop and provide a seamless experience for users of all levels.</p>
                                </div>
                            </li>
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <i class="fa fa-code fa-2x"></i>
                                </div>
                                <div class="feature-content">
                                    <h5>W3C Valid HTML code</h5>
                                    <p>We maintain all the standards mentioned by W3C. Each standard impacts to seo, page speed and crawling of the webpage.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="device-showcase">
            <div class="devices">
                <div class="ipad-wrap wp1"></div>
                <div class="iphone-wrap wp2"></div>
            </div>
        </div>
        <div class="responsive-feature-img"><img src="<?=base_url('assets/img/devices.png')?>" alt="responsive devices"></div>
    </section>
    <section class="features-extra section-padding" id="assets">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="feature-list">
                        <h3>Main Features</h3>
                        <p>The best framework, tools and libraries included building the modern web application.</p>
                        <ul class="main_features">
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; Bootstrap 4x Stable</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; Light & Dark Version</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; Fully Responsive pages</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; 10+ Different Dashboards</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; 100+ Ready to used widget</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; Various Charts Options</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; Working Table Examples</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; Built in SASS</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; Ready to Use Application</li>
                            <li><i class="fa fa-star-o"></i>&nbsp;&nbsp; Detailed Documentation</li>
                        </ul>
                        <a href="#" class="btn btn-ghost btn-accent btn-small">Buy Now!</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="macbook-wrap wp3"></div>
        <div class="responsive-feature-img"><img src="<?=base_url('assets/img/macbook-pro.png')?>" alt="responsive devices"></div>
    </section>
    <section class="hero-strip section-padding">
        <div class="container">
            <div class="col-md-12 text-center">
                <h2>Why spend lots of time and money <br>on design when we have created one for you.</h2>
                <p>Don't believe us? Take a tour on your on and don't miss a perk.</p>
                <a href="https://themeforest.net/item/lucid-powerful-bootstrap-4-admin-webapp-template/22212271" class="btn btn-ghost btn-accent btn-large">Buy Now!</a>
            </div>
        </div>
    </section>
    <section class="demos section-padding" id="demo">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Dashboard Demos</h3>
                    <p>Lucid is fully professional, responsive, modern, multi-purpose and featured Admin template which can be used to create various website, Hospital Dashboard, University Dashboard, RealEstate Dashboard, Project Dashboard, eCommerce Dashboard, Analytical Dashboard, Blog Dashboard, File Manager Dashboard, Admin templates, Admin dashboards, Backend Websites, CMS, CRM or one can aldo build Blog, Business website and time line as well as portfolio. Lucid Admin makes the development process easy and fast for you and aims to help you implement your idea to real time.</p>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Analytical</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index1.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/index.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/index.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/index.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/index.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Demographic</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index2.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/index2.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/index2.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/index2.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/index2.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Hospital</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index3.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/index3.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/index3.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/index3.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/index3.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>University</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index4.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/index4.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/index4.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/index4.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/index4.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>RealEstate</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index5.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/index5.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/index5.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/index5.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/index5.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Project</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index6.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/index6.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/index6.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/index6.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/index6.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>eCommerce</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index8.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/index8.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/index8.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/index8.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/index8.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Blog</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index9.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/blog-dashboard.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/blog-dashboard.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/blog-dashboard.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/blog-dashboard.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>File Manager</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/demo/index10.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/file-dashboard.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/file-dashboard.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/file-dashboard.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/file-dashboard.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Widgets Demos</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Statistics</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/widgets/widgets1.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/widgets-statistics.html" target="_blank" class="btn btn-fill">Light Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/widgets-statistics.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/widgets-statistics.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/widgets-statistics.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Data</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/widgets/widgets2.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/widgets-data.html" target="_blank" class="btn btn-fill">View Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/widgets-data.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/widgets-data.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/widgets-data.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Chart</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/widgets/widgets3.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/widgets-chart.html" target="_blank" class="btn btn-fill">View Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/widgets-chart.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/widgets-chart.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/widgets-chart.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Weather</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/widgets/widgets4.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/widgets-weather.html" target="_blank" class="btn btn-fill">View Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/widgets-weather.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/widgets-weather.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/widgets-weather.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>Social</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/widgets/widgets5.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/widgets-social.html" target="_blank" class="btn btn-fill">View Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/widgets-social.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/widgets-social.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/widgets-social.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h5>eCommerce</h5>
                    <div class="demo-img">
                        <img alt="" src="<?=base_url('assets/img/widgets/widgets6.jpg')?>" class="img-responsive">
                        <div class="link-btn">
                            <a href="https://thememakker.com/templates/lucid/html/light/widgets-ecommerce.html" target="_blank" class="btn btn-fill">View Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/dark/widgets-ecommerce.html" target="_blank" class="btn btn-fill btn-dark">Dark Demo</a>
                            <a href="https://thememakker.com/templates/lucid/html/rtl/widgets-ecommerce.html" target="_blank" class="btn btn-fill">RTL Demo</a>
                            <br><br><a href="https://thememakker.com/templates/lucid/html/h-menu/widgets-ecommerce.html" target="_blank" class="btn btn-fill">Horizontal Menu</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="to-top">
        <div class="container">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="footer-links">
                        <ul class="footer-group">
                            <li><a href="https://thememakker.com/templates/lucid/html/light/index.html">Home</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/app-inbox.html">Inbox</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/app-chat.html">ChatApp</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/app-contact.html">Contact</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/app-taskboard.html">Taskboard</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/widgets-statistics.html">Statistics</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/blog-list.html">Blog</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/file-dashboard.html">File Manager</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/table-jquery-datatable.html">Jquery Datatable</a></li>
                            <li><a href="https://thememakker.com/templates/lucid/html/light/page-timeline.html">Timeline</a></li>
                        </ul>

                        <p>Copyright © 2018 <a href="#">Lucid</a></p>
                    </div>
                </div>
                <div class="social-share">
                    <p>Share Lucid with your friends</p>
                    <a href="https://twitter.com/thememakker" class="twitter-share"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.facebook.com/thememakkerteam" class="facebook-share"><i class="fa fa-facebook"></i></a>
                </div>
            </div>
        </div>
    </footer>
    <script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
     <script>window.jQuery || document.write('<script src="<?=base_url('assets/js/vendor/jquery-1.11.2.min.js');?>"></script>')</script>
    <script src="<?=base_url('assets/js/jquery.fancybox.pack.js')?>"></script>
    <script src="<?=base_url('assets/js/vendor/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/js/scripts.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery.flexslider-min.js')?>"></script>
    <script src="<?=base_url('assets/bower_components/classie/classie.js')?>"></script>
    <script src="<?=base_url('assets/bower_components/jquery-waypoints/lib/jquery.waypoints.min.js')?>"></script>


</body>

<!-- Mirrored from thememakker.com/templates/lucid/landing/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 03 Aug 2018 02:36:41 GMT -->
</html>
