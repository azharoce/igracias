<div class="header">
	<p class="lead">Login to your account</p>
</div>
<div class="body">
	<form class="form-auth-small" action="#" method="post">
		<div class="form-group">
			<label for="signin-email" class="control-label sr-only">Email</label>
			<input type="email" class="form-control" id="signin-email"  placeholder="Email">
		</div>
		<div class="form-group">
			<label for="signin-password" class="control-label sr-only">Password</label>
			<input type="password" class="form-control" id="signin-password"  placeholder="Password">
		</div>
		<div class="form-group clearfix">
			<label class="fancy-checkbox element-left">
				<input type="checkbox">
				<span>Remember me</span>
			</label>
		</div>
		<button type="button" onclick="doLogin()" class="btn btn-primary btn-lg btn-block">LOGIN</button>
		<div class="bottom">
			<span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="#" onclick="change_content('forgot_password','_login')">Forgot password?</a></span>
			<span>Don't have an account? <a href="#"  onclick="change_content('register','_login')">Register</a></span>
		</div>
	</form>
</div>