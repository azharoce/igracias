
                        <div class="header">
                            <p class="lead">Recover my password</p>
                        </div>
                        <div class="body">
                            <p>Please enter your email address below to receive instructions for resetting password.</p>
                            <form class="form-auth-small" action="#" method="post">
                                <div class="form-group">
                                    <input type="password" class="form-control" id="signup-password" placeholder="Password">
                                </div>
                                <button type="button" onclick="login()" class="btn btn-primary btn-lg btn-block">RESET PASSWORD</button>
                                <div class="bottom">
                                    <span class="helper-text">Know your password? <a href="#"  onclick="change_content('login','_login')">Login</a></span>
                                </div>
                            </form>
                        </div>
                  