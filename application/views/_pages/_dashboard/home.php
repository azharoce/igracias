        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Dashboard</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index-2.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ul>
                    </div>            
                    <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                        <div class="bh_chart hidden-xs">
                            <div class="float-left m-r-15">
                                <small>Visitors</small>
                                <h6 class="mb-0 mt-1"><i class="icon-user"></i> 1,784</h6>
                            </div>
                            <span class="bh_visitors float-right"><canvas width="47" height="42" style="display: inline-block; width: 47px; height: 42px; vertical-align: top;"></canvas></span>
                        </div>
                        <div class="bh_chart hidden-sm">
                            <div class="float-left m-r-15">
                                <small>Visits</small>
                                <h6 class="mb-0 mt-1"><i class="icon-globe"></i> 325</h6>
                            </div>
                            <span class="bh_visits float-right"><canvas width="41" height="42" style="display: inline-block; width: 41px; height: 42px; vertical-align: top;"></canvas></span>
                        </div>
                        <div class="bh_chart hidden-sm">
                            <div class="float-left m-r-15">
                                <small>Chats</small>
                                <h6 class="mb-0 mt-1"><i class="icon-bubbles"></i> 13</h6>
                            </div>
                            <span class="bh_chats float-right"><canvas width="47" height="42" style="display: inline-block; width: 47px; height: 42px; vertical-align: top;"></canvas></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-3 col-md-6">
                    <div class="card top_counter">
                        <div class="body">
                            <div class="icon"><i class="fa fa-user"></i> </div>
                            <div class="content">
                                <div class="text">New Employee</div>
                                <h5 class="number">22</h5>
                            </div>
                            <hr>
                            <div class="icon"><i class="fa fa-users"></i> </div>
                            <div class="content">
                                <div class="text">Total Employee</div>
                                <h5 class="number">425</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card top_counter">
                        <div class="body">
                            <div class="icon"><i class="fa fa-university"></i> </div>
                            <div class="content">
                                <div class="text">Total Salary</div>
                                <h5 class="number">$2.8M</h5>
                            </div>
                            <hr>
                            <div class="icon"><i class="fa fa-university"></i> </div>
                            <div class="content">
                                <div class="text">Avg. Salary</div>
                                <h5 class="number">$1,250</h5>
                            </div>
                        </div>
                    </div>
                </div>
                     <div class="col-lg-5 col-md-12">

                    <div class="card">
                        <div class="body">
                            <ul class="nav nav-tabs-new">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Overview">Overview</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#password">Password</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Themes">Themes</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Settings">Settings</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="tab-content padding-0">

                        <div class="tab-pane active" id="Overview">
                            <div class="card">
                                <div class="body">
                                    <div class="new_post">
                                        <div class="form-group">
                                            <textarea rows="7" class="form-control no-resize" placeholder="Please type what you want..."></textarea>
                                        </div>
                                        <div class="post-toolbar-b">
                                            <button class="btn btn-warning"><i class="icon-link"></i></button>
                                            <button class="btn btn-warning"><i class="icon-camera"></i></button>
                                            <button class="btn btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>

                        <div class="tab-pane" id="Themes">

                            <div class="card">
                                <div class="body">
                                    <h6>Themes Setting</h6>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="form-group">                                                
                                                <input type="password" class="form-control" placeholder="Old Password">
                                            </div>
                                            <div class="form-group">                                                
                                                <input type="password" class="form-control" placeholder="New Password">
                                            </div>
                                            <div class="form-group">                                                
                                                <input type="password" class="form-control" placeholder="Re New Password">
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <button type="button" class="btn btn-primary">Update</button> &nbsp;&nbsp;
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                            </div>

                           
                        </div>
                        <div class="tab-pane" id="password">

                            <div class="card">
                                <div class="body">
                                    <h6>Reset Password</h6>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="form-group">                                                
                                                <input type="password" class="form-control" placeholder="Old Password">
                                            </div>
                                            <div class="form-group">                                                
                                                <input type="password" class="form-control" placeholder="New Password">
                                            </div>
                                            <div class="form-group">                                                
                                                <input type="password" class="form-control" placeholder="Re New Password">
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <button type="button" class="btn btn-primary">Update</button> &nbsp;&nbsp;
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                            </div>

                           
                        </div>

                    </div>
                    
                </div>         
            </div>

         

            <div class="row">
                <div class="col-lg-8 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h2>Employee Performance</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0">
                                    <thead class="thead-dark">
                                        <tr>
                                        <th>Avatar</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Performance</th>
                                        <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img src="assets/img/xs/avatar1.jpg" class="rounded-circle width35" alt=""></td>
                                            <td>Marshall Nichols</td>
                                            <td><span>UI UX Designer</span></td>
                                            <td><span class="badge badge-success">Good</span></td>
                                            <td><span class="sparkbar"><canvas width="34" height="16" style="display: inline-block; width: 34px; height: 16px; vertical-align: top;"></canvas></span></td>
                                        </tr>
                                        <tr>
                                            <td><img src="assets/img/xs/avatar2.jpg" class="rounded-circle width35" alt=""></td>
                                            <td>Susie Willis</td>
                                            <td><span>Designer</span></td>
                                            <td><span class="badge badge-warning">Average</span></td>
                                            <td><span class="sparkbar"><canvas width="34" height="16" style="display: inline-block; width: 34px; height: 16px; vertical-align: top;"></canvas></span></td>
                                        </tr>
                                        <tr>
                                            <td><img src="assets/img/xs/avatar3.jpg" class="rounded-circle width35" alt=""></td>
                                            <td>Francisco Vasquez</td>
                                            <td><span>Team Leader</span></td>
                                            <td><span class="badge badge-primary">Excellent</span></td>
                                            <td><span class="sparkbar"><canvas width="34" height="16" style="display: inline-block; width: 34px; height: 16px; vertical-align: top;"></canvas></span></td>
                                        </tr>
                                        <tr>
                                            <td><img src="assets/img/xs/avatar4.jpg" class="rounded-circle width35" alt=""></td>
                                            <td>Erin Gonzales</td>
                                            <td><span>Android Developer</span></td>
                                            <td><span class="badge badge-danger">Weak</span></td>
                                            <td><span class="sparkbar"><canvas width="34" height="16" style="display: inline-block; width: 34px; height: 16px; vertical-align: top;"></canvas></span></td>
                                        </tr>
                                        <tr>
                                            <td><img src="assets/img/xs/avatar5.jpg" class="rounded-circle width35" alt=""></td>
                                            <td>Ava Alexander</td>
                                            <td><span>UI UX Designer</span></td>
                                            <td><span class="badge badge-success">Good</span></td>
                                            <td><span class="sparkbar"><canvas width="34" height="16" style="display: inline-block; width: 34px; height: 16px; vertical-align: top;"></canvas></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5">
                    <div class="card">
                        <div class="header">
                            <h2>Employee Structure</h2>
                        </div>
                        <div class="body text-center">
                            <h6>Male</h6>
                            <div style="display:inline;width:125px;height:125px;"><canvas width="125" height="125"></canvas><input type="text" class="knob2" data-linecap="round" value="73" data-width="125" data-height="125" data-thickness="0.15" data-fgcolor="#49a9e5" readonly="readonly" style="width: 66px; height: 41px; position: absolute; vertical-align: middle; margin-top: 41px; margin-left: -95px; border: 0px; background: none; font: bold 25px Arial; text-align: center; color: rgb(73, 169, 229); padding: 0px; -webkit-appearance: none;"></div>
                            <hr>
                            <h6>Female</h6>
                            <div style="display:inline;width:125px;height:125px;"><canvas width="125" height="125"></canvas><input type="text" class="knob2" data-linecap="round" value="27" data-width="125" data-height="125" data-thickness="0.15" data-fgcolor="#b880e1" readonly="readonly" style="width: 66px; height: 41px; position: absolute; vertical-align: middle; margin-top: 41px; margin-left: -95px; border: 0px; background: none; font: bold 25px Arial; text-align: center; color: rgb(184, 128, 225); padding: 0px; -webkit-appearance: none;"></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
